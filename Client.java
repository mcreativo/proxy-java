import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Client
{
    public static void main(String[] args)
    {
        List<Subject> subjects = new ArrayList<>();
        Stream.generate(Subject::new).limit(500).forEach(subjects::add);

        //some code

        subjects.stream().limit(10).forEach(Subject::doSomething);
    }
}
