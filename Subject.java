public class Subject
{
    public Subject()
    {
        try
        {
            Thread.sleep(10);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void doSomething()
    {
        System.out.println("doing something");
    }
}
